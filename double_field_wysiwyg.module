<?php
/**
 * @file
 * This module allows Double field textareas to become WYSIWYG editors. It
 * addresses the need for paired fields comprising a title and a table.
 *
 * Double field module was chosen over the following alternatives:
 *  - Field collection, which allows WYSIWYG out of the box but is incompatible
 *    with translation.
 *  - Definition field, which has no advantages and is not as nice to use.
 *  - Field group multiple, which loses data on re-saving and groups the fields
 *    the wrong way (all titles, then all tables).
 *
 * The current implementation allows the user to specify an input format for
 * each Double field textarea; converts non-plaintext textareas into WYSIWYGS
 * via a form alter, converts them back on form submission, and finally
 * overrides the theme to render HTML rather than plaintext.
 *
 * Another approach might be to alter (is override possible?) Double field
 * itself so that WYSIWYG is an explicit option, however attempts at this led to
 * a query error on submission, as the number of values (including format) does
 * not match the number of columns. The query does not seem available to be
 * altered via hook_query_alter(), hence this approach was shelved.
 */

// done: Admin interface to control which textareas become WYSIWYGS.
//  - Can this be added as an input format select on the existing field settings
//    form? With plain text as the default option, meaning this module can be
//    applied or not at CT creation time.
// todo: More restricted WYSIWYG profile for table entry.
// todo: Hide input format options from users,
// todo: Custom accordion, not OOTB accordion.
// done: Limit filter options to actual filters.
// done: not restrict to second position.

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Performs a quick and dirty conversion of textareas in double fields on a node
 * form to WYSIWYG editors. Adds a submit handler for reverse conversion before
 * saving.
 */
function double_field_wysiwyg_form_node_form_alter(&$form, &$form_state) {
  $node_type = $form['type']['#value'];
  $double_field_textareas = _double_field_wysiwyg_get_double_field_textareas($form);

  // Check each double field for textareas with non-plaintext formats. If found,
  // convert those textareas to wysiwygs with appropriate formats. Maintain a
  // list of wysiwyg field positions to pass to the submit handler.
  $double_field_wysiwygs = array();
  foreach ($double_field_textareas as $field_name => $positions) {
    foreach ($positions as $position) {
      $format = _double_field_wysiwyg_get_format($field_name, $node_type, $position); // todo comment function that plain_text is default.
      if ($format != 'plain_text') {
        foreach (range(0, $form[$field_name]['und']['#max_delta']) as $delta) {
          _double_field_wysiwyg_convert_textarea_to_wysiwyg($form[$field_name]['und'][$delta][$position], $format);
        }
        $double_field_wysiwygs[$field_name][] = $position;
      }
    }
  }

  // If at least one wysiwyg field was found, add a submit handler, and add the
  // list of wysiwyg field positions to the form so the handler will have them.
  if (!empty($double_field_wysiwygs)) {
    $form['#submit'][] = 'double_field_wysiwyg_node_form_submit';
    $form['#double_field_wysiwygs'] = $double_field_wysiwygs;
  }
}

/**
 * Helper function, identifies double field textarea fields on a form.
 *
 * @param $form
 *   The form.
 * @return array
 *   The field names of double field textarea fields, else an empty array
 *   if none.
 */
function _double_field_wysiwyg_get_double_field_textareas($form) {
  $textareas = array();
  foreach (array_keys($form) as $key) {
    if (preg_match('#^field_#', $key) and in_array('field-type-double-field', $form[$key]['#attributes']['class'])) {
      if (strpos($form[$key]['#attributes']['class'][2], 'textarea--') !== FALSE) {
        $textareas[$key][] = 'first';
      }
      if (strpos($form[$key]['#attributes']['class'][2], '--textarea') !== FALSE) {
        $textareas[$key][] = 'second';
      }
    }
  }
  return $textareas;
}

/**
 * Helper function, converts a textarea on a form to a wysiwyg with a
 * given format.
 *
 * @param $textarea
 *   The textarea.
 * @param $format
 *   The format.
 */
function _double_field_wysiwyg_convert_textarea_to_wysiwyg(&$textarea, $format) {
  $textarea['#type'] = 'text_format';
  if (!isset($textarea['#default_value'])) {
    $textarea['#default_value'] = $textarea['value']['#default_value'];
  }
  $textarea['#rows'] = 10;
  $textarea['#format'] = $format;
  $textarea['#base_type'] = 'textarea';
  $textarea['#attributes']['class'][0] = 'text-full';
  unset($textarea['value']);
}

/**
 * Submit handler for the node form.
 *
 * Converts double field WYSIWYG editors back to textareas before saving.
 */
function double_field_wysiwyg_node_form_submit(&$form, &$form_state) {
  foreach ($form['#double_field_wysiwygs'] as $field_name => $positions) {
    foreach (range(0, $form[$field_name]['und']['#max_delta']) as $delta) {
      foreach ($positions as $position) {

        // Return format to textarea and remove format from submitted values. If
        // not done, format will be added to the query args and then these will
        // mismatch the column count.
        $form[$field_name]['und'][$delta][$position]['#type'] = 'textarea';
#SV think every textarea has formats now, $position is the better indicator.
#        if (isset($form[$field_name]['und'][$delta][$position]['#format'])) {
          unset($form[$field_name]['und'][$delta][$position]['#format']);
          unset($form_state['values'][$field_name]['und'][$delta][$position]['format']);
#        }
      }

      // Remove empty rows from field. If not done manually, empty rows will be
      // added to field on every node submit.
      if (empty($form_state['values'][$field_name]['und'][$delta]['first']['value']) and empty($form_state['values'][$field_name]['und'][$delta]['second']['value'])) {
        unset($form_state['values'][$field_name]['und'][$delta]);
      }
    }
  }
}

/**
 * Implements hook_theme_registry_alter().
 */
function double_field_wysiwyg_theme_registry_alter(&$theme_registry) {
  $theme_registry['double_field']['theme path'] = drupal_get_path('module', 'double_field_wysiwyg');
  $theme_registry['double_field']['function'] = 'double_field_wysiwyg_double_field';
  $theme_registry['double_field_accordion']['theme path'] = drupal_get_path('module', 'double_field_wysiwyg');
  $theme_registry['double_field_accordion']['function'] = 'double_field_wysiwyg_double_field_accordion';
}

/**
 * Overrides theme_double_field().
 *
 * Decodes HTML entities from double field second values so that e.g. HTML
 * tables are rendered correctly.
 */
function double_field_wysiwyg_double_field($vars) {
  // todo check.
  $vars['element']['#item']['first'] = decode_entities($vars['element']['#item']['first']);
  $vars['element']['#item']['second'] = decode_entities($vars['element']['#item']['second']);
  $output = theme_double_field($vars);
  return $output;
}

/**
 * Overrides theme_double_field_accordion().
 *
 * Decodes HTML entities from double field second values so that e.g. HTML
 * tables are rendered correctly.
 */
function double_field_wysiwyg_double_field_accordion($vars) {
  // todo check
  foreach ($vars['element']['#items'] as &$item) {
    $item['first'] = decode_entities($item['first']);
    $item['second'] = decode_entities($item['second']);
  }
  $output = theme_double_field_accordion($vars);
  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Alters the field edit form for double field fields so that textareas have a
 * select for input format.
 */
function double_field_wysiwyg_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  if ($form['#field']['type'] == 'double_field') {

    // Determine which positions hold textareas.
    $textarea_positions = array();
    if (strpos($form['#instance']['widget']['type'], 'textarea_') !== FALSE) {
      $textarea_positions[] = 'first';
    }
    if (strpos($form['#instance']['widget']['type'], '_textarea') !== FALSE) {
      $textarea_positions[] = 'second';
    }

    // If we have textareas:
    if (!empty($textarea_positions)) {

      // 1. Prepare format select options.
      $options = array();
      foreach (filter_formats() as $key => $format) {
        $options[$key] = $format->name;
      }

      // 2. For each textarea, add a select for input format.
      foreach ($textarea_positions as $position) {
        $default_option = _double_field_wysiwyg_get_format($form['#instance']['field_name'], $form['#instance']['bundle'], $position);
        $form['instance']['widget']['settings'][$position]['textarea']['format'] = array(
          '#type' => 'select',
          '#title' => t('Input format'),
          '#options' => $options,
          '#default_value' => $default_option,
        );
      }

      // 3. Add submit handler and save positions to form so handler can reuse.
      $form['#textarea_positions'] = $textarea_positions;
      $form['#submit'][] = 'double_field_wysiwyg_form_field_ui_field_edit_form_submit';
    }
  }
}

/**
 * Submit handler for the field edit form.
 *
 * Saves formats set for double field textareas.
 */
function double_field_wysiwyg_form_field_ui_field_edit_form_submit(&$form, &$form_state) {
  $instance = $form_state['values']['instance'];
  foreach ($form['#textarea_positions'] as $position) {
    _double_field_wysiwyg_set_format($instance['field_name'], $instance['bundle'], $position, $instance['widget']['settings']['second']['textarea']['format']);
  }
}

/**
 * Helper function, gets the format for a double field textarea.
 *
 * @param $field_name
 *   The name of the field.
 * @param $node_type
 *   The node type the instance of the field applies to.
 * @param $position
 *   The position ('first' or 'second') of the textarea within the double field.
 * @return string
 *   The format, defaults to 'plain_text' if one has not been set.
 */
function _double_field_wysiwyg_get_format($field_name, $node_type, $position) {
  $formats = variable_get('double_field_wysiwyg_instance_formats', array());
  return isset($formats[$field_name][$node_type][$position]) ? $formats[$field_name][$node_type][$position] : 'plain_text';
}

/**
 * Helper function, sets the format for a double field textarea.
 *
 * @param $field_name
 *   The name of the field.
 * @param $node_type
 *   The node type the instance of the field applies to.
 * @param $position
 *   The position ('first' or 'second') of the textarea within the double field.
 * @param $format
 *   The name of the format.
 */
function _double_field_wysiwyg_set_format($field_name, $node_type, $position, $format) {
  $formats = variable_get('double_field_wysiwyg_instance_formats', array());
  $formats[$field_name][$node_type][$position] = $format;
  variable_set('double_field_wysiwyg_instance_formats', $formats);
}